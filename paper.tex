\documentclass{sig-alternate}
\usepackage[colorlinks]{hyperref}
\usepackage[caption=false]{subfig}
\usepackage{tabu}
\usepackage{enumitem}

\begin{document}
%
% --- Author Metadata here ---
\conferenceinfo{MSR}{'14 Hyderabad, India}
%\CopyrightYear{2007} % Allows default copyright year (20XX) to be over-ridden - IF NEED BE.
%\crdata{0-12345-67-8/90/01}  % Allows default copyright data (0-89791-88-6/97/05) to be over-ridden - IF NEED BE.
% --- End of Author Metadata ---

\title{How is Software Changed at a Fine-Grained Level?}


\numberofauthors{2} %  in this sample file, there are a *total*
% of EIGHT authors. SIX appear on the 'first-page' (for formatting
% reasons) and the remaining two appear in the \additionalauthors section.
%
\author{
% You can go ahead and credit any number of authors here,
% e.g. one 'row of three' or two rows (consisting of one row of three
% and a second row of one, two or three).
%
% The command \alignauthor (no curly braces needed) should
% precede each author name, affiliation/snail-mail address and
% e-mail address. Additionally, tag each line of
% affiliation/address with \affaddr, and tag the
% e-mail address with \email.
%
% 1st. author
\alignauthor
Zhongpeng Lin\\
       \affaddr{Department of Computer Science}\\
       \affaddr{University of California, Santa Cruz, USA}\\
       \email{linzhp@soe.ucsc.edu}
% 2nd. author
\alignauthor
Jim Whitehead\\
       \affaddr{Department of Computer Science}\\
       \affaddr{University of California, Santa Cruz, USA}\\
       \email{ejw@soe.ucsc.edu}
}

\maketitle
\begin{abstract}
Understanding software evolution at a fine-grained level will lead to a better overall knowledge about software evolution.
To lay a foundation for the models of fine-grained code changes (FGCCs), we examine the distributions of different metrics based on FGCCs.
The data from four different software projects shows that
FGCC-based code churn metrics follow double Pareto distributions, which have a log-normal bodies and power law tails, while change types follow the 80-20 rule.
The findings in this paper provide an empirical background on code change distributions for future software evolution models.
\end{abstract}

\keywords{Software Evolution, Code Churn, Double Pareto Distributions, Fine-grained Code Changes}

\section{Introduction}
Software evolution has been a research topic since the 1970s.
Many of the emphases in software evolution studies have been on the high level processes, such as the evolution of files \cite{Wu2007}, modules \cite{Louridas2008} or even projects \cite{Lehman1996}.
These studies greatly advanced human knowledge on software evolution, but there is abundant useful information about low level code changes as well.
Most of the low level change analyses to date were performed on single lines or on a small number of lines of code (e.g., \cite{Gorshenev2004}), treating the source code as plain text.
However, source code is actually semi-structured text; a valid piece of source code can be deterministically parsed into an abstract syntax tree (AST).
Developing a richer understanding of the change processes on ASTs is very important.
Statistically, one would expect the textual and AST changes to be highly correlated, but because AST changes are more structural, and less sensitive to trivial changes such as code formatting, they are a more accurate representation of the low level evolutionary process that is occurring.
What is more, some useful information about the changes is not readily available in textual changes. 
For example, if one wants to know how frequent loop conditions are changed, textual comparison alone does not give such information.

In this study, an AST differencing tool, ChangeDistiller \cite{Fluri2007}, is used to compare ASTs in different source code revisions and extract fine-grained code changes (FGCCs).
An FGCC has information about the type of the changed AST node (e.g., \texttt{if} statement, method), the parent and the children of the node, and the change type (e.g., \textsc{method renaming, additional class}). 
It also has information about the position of the node in the source code, allowing for inspection at even finer granularity if necessary.

It has been discovered that power law and double Pareto distributions are pervasive in software systems at different levels of abstraction, across application domains and languages \cite{Herraiz2007a,Louridas2008}.
One would expect similar distributions in FGCC metrics, which leads to the hypotheses of this study:

\begin{enumerate}[label=H\arabic*, nosep=true]
	\item\label{hypo:rev} Most revisions comprise small numbers of changes, and only a few revisions have large numbers of changes. 
	The number of FGCCs per revision follows a double Pareto distribution.
	\item\label{hypo:month} The number of FGCCs made each month is relatively small, but there are a few months during which large numbers of FGCCs were made.
	The distribution of monthly code churn is also a double Pareto distribution.
	\item\label{hypo:file} Most files have small numbers of changes through out their history, and only a few files have large numbers of changes.
	The number of FGCCs per file follows a double Pareto distribution.
	\item\label{hypo:type} Most FGCCs come from a small group of change types, exhibiting the 80-20 rule.
\end{enumerate}

This paper is organized as follows: Section~\ref{sec:related} reviews some related work. The data we use for this study are described in Section~\ref{sec:collection}, followed by analyses of the data which in fact verify our hypotheses (Section~\ref{sec:analysis}). Section~\ref{sec:conclusion} discusses our findings and concludes the paper.

\section{Related Work}
\label{sec:related}
Many studies on low level software evolutionary processes are based on textual changes.
For example, Gorshenev et al. \cite{Gorshenev2004} studied the distribution of the number of added and deleted lines of code, and proposed a theoretic model based on self-organized criticality for software evolution.
Another example is the study of co-changed lines by Zimmermann et al. \cite{Zimmermann2006}.
The shortcoming of textual changes is they are often inaccurate in representing the actual changes, and the information extracted from such changes is very limited.

Others have examined low level structural changes. 
For instance, Kim et al. \cite{Kim2006d} studied changes in Java micro patterns during software evolution and identified the bug-prone changes.
Comparing to that, the FGCCs studied in this paper provide a more detailed view of software evolution.
The signature changes studied by Kim et al. \cite{Kim2006a} are similar to the FGCCs studied in this paper.
However, the former changes were extracted by manual inspection, which limits both the types and the number of changes that can be examined.
ChangeDistiller \cite{Fluri2007} provides an opportunity to examine FGCCs at a larger scale.
Since its introduction, studies have shown the potential of FGCCs in software engineering research, for example when used for bug prediction \cite{Giger2011a}.
However, the distributions of FGCC-based metrics have not been investigated.

\section{Data Collection}
\label{sec:collection}
All data in this study come from four open source Java projects. 
A fork of CVSAnalY\footnote{\url{github.com/linzhp/CVSAnalY/tree/develop}} 
was used to collect data from their Git repositories.
Table~\ref{tab:projects} shows the start and end dates of the data collection, as well as the number of revisions collected. 
Only revisions in their master branches were collected for this study.
However, some of these revisions might have been developed in other branches before being merged into the master branch. 
As Git keeps the branching and merging history, the revisions in the master branch of a project form a graph, in which multiple revisions may be modified from the same revision.
On the other hand, a revision may be the result of merging two previous revisions due to parallel development.
Changes in such merging revisions are ignored in this study.

\begin{table}
	\centering
	\caption{Data sources}
	\label{tab:projects}
	\begin{tabular}{cccc}
	\hline
	Project		& Start Date & End Date	  & Revisions \\ 
	\hline
	jEdit		& 1998-09-27 & 2012-08-08 & 6486 \\ 
	Eclipse JDT	& 2001-06-05 & 2013-09-09 & 19321 \\ 
	Apache Maven& 2003-09-01 & 2014-01-29 & 9723 \\ 
	Google Guice& 2006-08-22 & 2013-12-11 & 1198 \\ 
	\hline
	\end{tabular} 
\end{table}

After collecting the raw data, we used ChangeDistiller to extract the differences in the ASTs of each revision from the previous one. 
As ChangeDistiller only supports Java, the FGCCs studied in this research are all Java code changes.

\section{Data Analysis}
\label{sec:analysis}
In this section, we analyze the distributions of different code churn metrics and changes types. 
Code churn is used to measure the number of changes to a software unit over time.
Previously, the number of changes was often based on lines of code modified (e.g., \cite{Nagappan2005}). 
In this study, we instead use FGCCs to calculate code churn metrics.
The period of time during which code churn is calculated can be measured by revisions in version control systems or wall-time units.
Meanwhile, a software unit can be a software project or a source code file, etc.
Some of these variations are analyzed in this section.

Newman \cite{Newman2005} suggests that the best way to visualize a power law distribution is plotting its complementary cumulative distribution function (CCDF) in logarithmic scale.
The CCDF of a power law distribution should form a straight line, usually with a negative slope.
In order to find out the distribution of a metric, we first plot its CCDF.
If the body deviates from the straight line, we then take the base-2 logarithm of the metric and compare it with the standard normal distribution using a Quantile-Quantile (Q-Q) plot.
A straight line in the Q-Q plot would suggest a distribution of is log-normal.
If the tails of a metric distribution follow the power law while the body is log-normal, the distribution is double Pareto.

\subsection{Revision-based Code Churn}
\label{sec:rev}
In this section, we count the number of FGCCs for each revision, and use it as the measure of code churn.

Not all revisions have FGCCs, because some of them do not modify any Java code, and some others only have code formatting changes.
Revisions without any FGCCs are ignored in this study.
To verify \ref{hypo:rev}, we calculated the order statistics for revision-based code churn.
Table~\ref{tab:changesPerCommit} shows that in all projects, about 80\% of revisions have less than 100 FGCCs, while a small number of revisions have up to 40,551 FGCCs.
\begin{table}
	\centering
	\caption{Order statistics for revision code churn}
	\label{tab:changesPerCommit}
	\begin{tabular}{ccccc}
	\hline
	Project		& Min. & Median & 80\% Quantile & Max. \\ 
	\hline
	jEdit		& 1 & 16	& 76.6	& 6960 \\ 
	Eclipse JDT & 1	& 14	& 59	& 40551 \\ 
	Apache Maven& 1	& 12	& 49	& 5989 \\ 
	Google Guice& 1	& 26.5	& 101.6	& 3130 \\ 
	\hline
	\end{tabular} 
\end{table}

To find out whether the distributions follow the power law, the CCDF for each project is plotted in Figure~\ref{fig:changePerCommitccdf}.
On all of the four plots, there are fluctuations at the right tails, due to the scarcity of the revisions with large number of FGCCs.
Nevertheless, it can still be seen that the tails are roughly along straight lines, indicating the existence of power law distributions.
What is more, bodies in all plots have curved shapes and thus do not follow the power law.
We then compared the base-2 logarithm of the code churn with the standard normal distribution in Q-Q plots (Figure~\ref{fig:changesPerCommitqq}).
On each Q-Q plot, most of the points are on a straight line, while both tails deviate.
The straight lines suggest that the distributions have log-normal bodies.
The bottom-left part of each plot is broken into several horizontal lines, indicating a non-trivial number of revisions having one, two or three FGCCs.
Among these lines, the one representing revisions with one FGCC is the longest, and the length decreases quickly as the number of FGCCs increases.
The horizontal lines are invisible for the revisions with more than three FGCCs.
The fast decreasing number of revisions on the horizontal lines is an indicator of power law distribution in the tail.

With both tails following the power law, and the body being log-normal, the revision-based code churn of each project is a double Pareto distribution, confirming \ref{hypo:rev}.

\begin{figure}
	\centering
	\subfloat[jEdit]{
		\includegraphics[width=0.47\columnwidth]{changesPerCommit/ccdf/jedit}
	}
	\subfloat[Eclipse JDT]{
		\includegraphics[width=0.47\columnwidth]{changesPerCommit/ccdf/jdt}
	}
	\\
	\subfloat[Apache Maven]{
		\includegraphics[width=0.47\columnwidth]{changesPerCommit/ccdf/maven}
	}
	\subfloat[Google Guice]{
		\includegraphics[width=0.47\columnwidth]{changesPerCommit/ccdf/guice}
	}
	\caption{CCDF for revision-based code churn}
	\label{fig:changePerCommitccdf}
\end{figure}
\begin{figure}
	\centering
	\subfloat[jEdit]{
		\includegraphics[width=0.47\columnwidth]{changesPerCommit/qq/jedit}
	}
	\subfloat[Eclipse JDT]{
		\includegraphics[width=0.47\columnwidth]{changesPerCommit/qq/jdt}
	}
	\\
	\subfloat[Apache Maven]{
		\includegraphics[width=0.47\columnwidth]{changesPerCommit/qq/maven}
	}
	\subfloat[Google Guice]{
		\includegraphics[width=0.47\columnwidth]{changesPerCommit/qq/guice}
	}
	\caption{Normal Q-Q plots for revision-based code churn distribution}
	\label{fig:changesPerCommitqq}
\end{figure}

\subsection{Monthly Code Churn}
In this section, FGCCs are binned according to the calendar months when they were made, and the number of FGCCs for each month is used as the measure of monthly code churn.

The order statistics of monthly code churn are shown in Table~\ref{tab:changesPerMonth}. Compared to Table~\ref{tab:changesPerCommit}, the variance of monthly code churn is less than that of revision code churn for each project.
Take jEdit for example, the maximal monthly code churn is only about four times as the 80\% quantile, but the revision with most FGCCs has 90 times as many FGCCs as the 80\% quantile revision.

\begin{table}
	\centering
	\caption{Order statistics for monthly code churn}
	\label{tab:changesPerMonth}
	\begin{tabular}{ccccc}
	\hline
	Project		& Min.	& Median& 80\% Quantile & Max. \\ 
	\hline
	jEdit		& 20	& 1090	& 2923.2	& 12593 \\ 
	Eclipse JDT & 28	& 4037.5& 10552.2	& 48126 \\ 
	Apache Maven& 2		& 848	& 3791.8	& 13657 \\ 
	Google Guice& 1		& 428	& 1338		& 5684 \\ 
	\hline
	\end{tabular} 
\end{table}

In order to discover the shape of monthly code churn distribution, we plotted the CCDF and normal Q-Q plots for each project's monthly data. 
The CCDF plots for monthly code churn are similar to Figure~\ref{fig:changePerCommitccdf}, indicating they have power law tails, but due to the space limit, these monthly CCDF plots are not included here (see \cite{figures}).
Because there are often multiple revisions created in a month, the number of FGCCs in a month is generally more than the number in a revision.
Reflected on the normal Q-Q plots (Figure~\ref{fig:changesPerMonthqq}), there are very few months with only one, two or three FGCCs, so the horizontal lines in the bottom disappear.
The bodies still lie on straight lines, indicating that monthly code churn distributions have similar shapes to double Pareto. 
\ref{hypo:month} is thus verified.
\begin{figure}
	\centering
	\subfloat[jEdit]{
		\includegraphics[width=0.47\columnwidth]{changesPerMonth/qq/jedit}
	}
	\subfloat[Eclipse JDT]{
		\includegraphics[width=0.47\columnwidth]{changesPerMonth/qq/jdt}
	}
	\\
	\subfloat[Apache Maven]{
		\includegraphics[width=0.47\columnwidth]{changesPerMonth/qq/maven}
	}
	\subfloat[Google Guice]{
		\includegraphics[width=0.47\columnwidth]{changesPerMonth/qq/guice}
	}
	\caption{Normal Q-Q plots for monthly code churn distribution}
	\label{fig:changesPerMonthqq}
\end{figure}

\subsection{Code Churn Among Files}
In this section, we count the number of FGCCs for each file over the whole history of each project.
Non-Java files and untouched files are ignored.

Table~\ref{tab:changesPerFile} shows a similar pattern to Table~\ref{tab:changesPerCommit}.
Compared to the file with the most FGCCs, most files have very small numbers of FGCCs over their entire history.
The file with the most changes in each project had at least 30 times as many FGCCs as 80\% of the files in the same project. 
The order statistics for Eclipse JDT file changes are particular interesting.
As a project with a change history of more than 12 years, among more than 6000 Java files, the median number of FGCCs per file was only 8.
\begin{table}
	\centering
	\caption{Order statistics for code churn among files}
	\label{tab:changesPerFile}
	\begin{tabular}{ccccc}
	\hline
	Project		& Min. & Median & 80\% Quantile & Max. \\ 
	\hline
	jEdit		& 1 & 52	& 196	& 18123 \\ 
	Eclipse JDT & 1	& 8		& 85	& 31412 \\ 
	Apache Maven& 1	& 25	& 90	& 7592 \\ 
	Google Guice& 1	& 36	& 114	& 3492 \\ 
	\hline
	\end{tabular} 
\end{table}

The CCDFs and normal Q-Q plots of code churn among files have similar shapes to Figures~\ref{fig:changePerCommitccdf} and \ref{fig:changesPerCommitqq} respectively, confirming \ref{hypo:file}. The figures are omitted in the paper due to the space limit; interested readers are referred to \cite{figures}.

\subsection{Change Type Distribution}
When ChangeDistiller extracts FGCCs, it classifies the changes among 47 change types. 
\textsc{additional functionality} (method addition) and \textsc{removed object state} (field removal) are two example change types.
In this section, we calculate the frequencies for different change types, and examine their distribution.

As one might expect, \textsc{statement insert} is the most frequent change type in all projects.
Some other frequent change types are moving or deleting a statement, and adding or deleting fields, methods, Java Docs and comments.

There are some notable differences in the frequencies of some change types among the projects studied.
Google Guice and Apache Maven add method parameters more frequently than the other two projects,
while jEdit adds and deletes the \texttt{else} part of \texttt{if} statements more frequently than Google Guice.

In addition, we also found the most frequent nine changes types (20\% of 47 change types) in each project account for 85\% to 90\% of all changes\footnote{jEdit: 87.0\%, Eclipse JDT: 89.2\%, Apache Maven: 88.3\%, Google Guice: 88.3\%}, which is close to the 80-20 rule.
\ref{hypo:type} is thus verified.

\section{Discussion and Conclusion}
\label{sec:conclusion}
In this study, we examined fine-grained software changes in four open source projects and investigated the distribution of different code churn metrics based on FGCCs, as well as the distribution of change types.
The results show that all code churn metrics have double Pareto distributions, confirming \ref{hypo:rev}-\ref{hypo:file}.
Consequently, parameters for double Pareto distribution should be used to characterize FGCC-based code churn, and future models for fine-grained software evolution should have code churn following double Pareto distributions.

Monthly code churn having less variance than revision-based code churn may reflect the fact that the former is less sensitive to version control systems and development workflow.
For long-living projects, one can imagine that in early days when centralized version control systems such as Subversion were used,
developers had to complete a coding task, test it, and have the changes peer reviewed before submitting to the central repository for a new revision.
In that case, a revision might often have many changes.
After migrating the project to a distributed version control system such as Git, developers could create local intermediate revisions cheaply and thus frequently.
When a task is done, all local revisions are merged into the main repository\footnote{unless, for example in Git, a rebase scheme is used instead of merging}.
Such revisions tend to contain fewer FGCCs.
The mixture of revisions from different version control systems makes the number of FGCCs per revision fluctuate, while the monthly code churn is more stable.

We also found that the top 20\% of change types accounted for more than 85\% of changes in each project, having a similar distribution as the 80-20 rule (\ref{hypo:type}).
This fact should be taken into account in future software evolution research.

%
% The following two commands are all you need in the
% initial runs of your .tex file to
% produce the bibliography for the citations in your paper.
\bibliographystyle{abbrv}
\bibliography{Evolution,AST_Difference,additional}  % sigproc.bib is the name of the Bibliography in this case
% You must have a proper ".bib" file
%  and remember to run:
% latex bibtex latex latex
% to resolve all references
%
% ACM needs 'a single self-contained file'!
% \balancecolumns % GM June 2007
% That's all folks!
\end{document}
